import { BrowserModule } from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import { ReactiveFormComponent } from './reactive-form/reactive-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpService} from './service/http.service';
import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app.routing.module';
import {AngularFireModule} from '@angular/fire';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {LoginComponent} from './authorization/login/login.component';
import {SignupComponent} from './authorization/signup/signup.component';
import {AuthoeizationService} from './service/authoeization.service';
import {AuthQuardsService} from './service/auth-quards.service';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {TranslateService} from './service/translate.service';
import {TranslatePipe} from './pipe/translate.pipe';
import {HomeComponent} from './home/home.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

const config = {
  apiKey: 'AIzaSyBmg2sSBScEpDqSBAYr5DbMYGDYGBvUNsA',
  authDomain: 'cranes-20e46.firebaseapp.com',
  databaseURL: 'https://cranes-20e46.firebaseio.com',
  projectId: 'cranes-20e46',
  storageBucket: 'cranes-20e46.appspot.com',
  messagingSenderId: '273625077890'
};

export function setupTranslateFactory(
  service: TranslateService): Function {
  return () => service.use('en');
}

@NgModule({
  declarations: [
    AppComponent,
    ReactiveFormComponent,
    LoginComponent,
    SignupComponent,
    TranslatePipe,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(config),
    AngularFireAuthModule,
    AngularFirestoreModule,
    FormsModule,
    NgbModule
  ],
  providers: [
    HttpService,
    AuthoeizationService,
    AuthQuardsService,
    TranslateService,
    {
      provide: APP_INITIALIZER,
      useFactory: setupTranslateFactory,
      deps: [ TranslateService ],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
