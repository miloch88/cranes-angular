import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {AuthoeizationService} from '../../service/authoeization.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(private authService: AuthoeizationService, private router: Router) {
  }

  ngOnInit() {
  }

  signup(formData: NgForm) {
    this.authService.signup(formData.value.email, formData.value.password);
    this.router.navigate(['/']);
  }
}
