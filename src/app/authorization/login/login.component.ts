import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {AuthoeizationService} from '../../service/authoeization.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authService: AuthoeizationService, private router: Router) { }

  ngOnInit() {
  }

  login(formData: NgForm) {
    this.authService.login(formData.value.email, formData.value.password);
  }

  signInWithFacebook() {
    this.authService.signInWithFacebook()
      .then((res) => {
        this.router.navigate(['/']);
      })
      .catch((err) => console.log(err));
    this.router.navigate(['/']);
  }

  signInWithGoogle() {
    this.authService.signInWithGoogle()
      .then((res) => {
        this.router.navigate(['/']);
      })
      .catch((err) => console.log(err));
  }

}
