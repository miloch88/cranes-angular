import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

@Injectable()
export class HttpService {

  private hostUrl = 'http://35.227.80.107:8080/api/calculate';

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  constructor(private http: HttpClient) {
  }
  getCranesHeight(laod: number, radious: number, height: number) {

    const body = JSON.stringify({load: laod, radius: radious, height: height});

    return this.http.post<any>(this.hostUrl, body, this.httpOptions
    );
  }

  getCranes(laod: number, radious: number) {

    const body = JSON.stringify({load: laod, radius: radious});

    return this.http.post<any>(this.hostUrl, body, this.httpOptions
    );
  }

}

