import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthoeizationService} from './authoeization.service';

@Injectable()
export class AuthQuardsService implements CanActivate {

  constructor(private authService: AuthoeizationService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.authService.user) {
      return true;
    }
    return false;
  }
}
