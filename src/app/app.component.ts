import {Component, OnInit} from '@angular/core';
import {AuthoeizationService} from './service/authoeization.service';
import {Router} from '@angular/router';
import {AngularFireAuth} from '@angular/fire/auth';
import {TranslateService} from './service/translate.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  brandLang: any;
  defaultLang: any;

  langs = [
    {
      label: 'EN',
      value: 'en'
    },
    {
      label: 'PL',
      value: 'pl'
    }];

  constructor(public authService: AuthoeizationService,
              private router: Router,
              private _firebaseAuth: AngularFireAuth,
              private translate: TranslateService) {
  }

  logout() {
    this.authService.logout();
    this._firebaseAuth.auth.signOut();
    this.router.navigate(['/']);
  }

  setLang(lang: string) {
    this.translate.use(lang);
  }

  ngOnInit() {
    this.defaultLang = this.langs[0];
    this.brandLang = this.langs[0].value;
  }
}
