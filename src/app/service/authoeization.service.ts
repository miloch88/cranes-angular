import {Injectable} from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {User} from 'firebase';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';
import * as firebase from 'firebase';

@Injectable()
export class AuthoeizationService {

  private userFacebook: Observable<firebase.User>;
  private userDetails: firebase.User = null;
  user: User;

  constructor(private angularFire: AngularFireAuth, private router: Router) {
    angularFire.authState.subscribe(user => {
      this.user = user;
      this.userFacebook = angularFire.authState;

      this.userFacebook.subscribe(
        (user2) => {
          if (user2) {
            this.userDetails = user2;
            console.log(this.userDetails);
          } else {
            this.userDetails = null;
          }
        }
      );
    });
    this.userFacebook = angularFire.authState;
  }

  login(email: string, password: string) {
    this.angularFire.auth.signInWithEmailAndPassword(email, password)
      .then(user => {
        this.router.navigate(['/home']);
      }).catch(err => {
      console.log(err);
    });
  }

  signup(email: string, password: string) {
    this.angularFire.auth.createUserWithEmailAndPassword(email, password)
      .then(user => {
        console.log(user);
      })
      .catch(err => {
        console.log(err);
      });
    }

  logout() {
    this.angularFire.auth.signOut();
  }

  signInWithFacebook() {
    return this.angularFire.auth.signInWithPopup(
      new firebase.auth.FacebookAuthProvider()
    );
  }

  signInWithGoogle() {
    return this.angularFire.auth.signInWithPopup(
      new firebase.auth.GoogleAuthProvider()
    );
  }

}
