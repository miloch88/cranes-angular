import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {HttpService} from '../service/http.service';
import {HttpErrorResponse} from '@angular/common/http';
import {TranslateService} from '../service/translate.service';

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.css']
})
export class ReactiveFormComponent implements OnInit {

  allCranes$?: any = [];
  error?: any = [];

  unitsLoad = ['t', 'lb'];
  unitsRadius = ['m', 'ft'];
  unitsHeight = ['m', 'ft'];
  defaultLoad: any = 't';
  defaultRadius: any = 'm';
  defaultHeight: any = 'm';

  unitKinds: any = ['Metric', 'Imperial'];
  resultUnit: any = 'Metric';


  craneForm: FormGroup;
  crane = new CraneData();

  constructor(private httpService: HttpService) {
  }

  ngOnInit() {
    this.craneForm = new FormGroup({
      load: new FormControl(null, [Validators.required, Validators.min(0)]),
      radius: new FormControl(null, [Validators.required, Validators.min(0)]),
      height: new FormControl(null, Validators.min(0))
    });
  }

  setLoad(load: string) {
    this.defaultLoad = load;
  }

  setRadius(radius: string) {
    this.defaultRadius = radius;
  }

  setHeight(height: string) {
    this.defaultHeight = height;
  }

  setUnits(unit: string) {
    this.resultUnit = unit;
  }

  onSubmit() {
    this.allCranes$ = [];
    this.error = [];
    let loadForm;
    let radiusForm;
    let heightForm;

    if (this.defaultLoad === 'lb') {
      loadForm = this.craneForm.value.load / 2204.6244202;
    } else {
      loadForm = this.craneForm.value.load;
    }

    if (this.defaultRadius === 'ft') {
      radiusForm = this.craneForm.value.radius / 3.280839895;
    } else {
      radiusForm = this.craneForm.value.radius;
    }

    if (this.defaultHeight === 'ft') {
      heightForm = this.craneForm.value.height / 3.280839895;
    } else {
      heightForm = this.craneForm.value.height;
    }

    if (this.craneForm.value.height != null) {
      this.httpService.getCranesHeight(loadForm, radiusForm, heightForm)
        .subscribe(data => {
            if (data != null) {
              this.allCranes$.push(data);
            } else {
              this.error.push(1);
            }
          },
          (error: HttpErrorResponse) => {
            console.error(error.message);
          }
        );
    } else {
      this.httpService.getCranes(
        loadForm,
        radiusForm
      )
        .subscribe(data => {
            if (data != null) {
              this.allCranes$.push(data);
            } else {
              this.error.push(1);
            }
          },
          (error: HttpErrorResponse) => {
            console.error(error.status);
          }
        );
    }
  }
}

class CraneData {
  constructor(
    public load?: number,
    public radius?: number,
    public height?: number
  ) {
  }
}
